<?php
namespace EnGenius;

/**
 * 
 */
class EnGeniusGeoip
{

   public function __construct() {
      $this->regions = array(
         "0" => array(
            "optgroup" => __("Europe", "EnGenius"),
            "options" => array(
               "0" => array(
                  "text" => __("English", "EnGenius"),
                  "href" => __("https://www.engeniusnetworks.eu/?disable_geoip", "EnGenius")
               ),
               // "1" => array(
               //    "text" => __("English (UK)", "EnGenius"),
               //    "href" => __("https://www.engeniusnetworks.eu/uk/?disable_geoip", "EnGenius")
               // ),
               "2" => array(
                  "text" => __("Deutsch", "EnGenius"),
                  "href" => __("https://www.engeniusnetworks.eu/de/?disable_geoip", "EnGenius")
               ),
               "3" => array(
                  "text" => __("Español", "EnGenius"),
                  "href" => __("https://www.engeniusnetworks.eu/es/?disable_geoip", "EnGenius")
               ),
               "4" => array(
                  "text" => __("Italiano", "EnGenius"),
                  "href" => __("https://www.engeniusnetworks.eu/it/?disable_geoip", "EnGenius")
               ),
               "5" => array(
                  "text" => __("русский язык", "EnGenius"),
                  "href" => __("https://www.engeniusnetworks.eu/ru/?disable_geoip", "EnGenius")
               ),
               "6" => array(
                  "text" => __("Français", "EnGenius"),
                  "href" => __("https://www.engeniusnetworks.eu/fr/?disable_geoip", "EnGenius")
               )
               // ,
               // "7" => array(
               //    "text" => __("polski", "EnGenius"),
               //    "href" => __("https://www.engeniusnetworks.eu/pl/?disable_geoip", "EnGenius")
               // ),
               // "8" => array(
               //    "text" => __("svenska", "EnGenius"),
               //    "href" => __("https://www.engeniusnetworks.eu/sv/?disable_geoip", "EnGenius")
               // )
            )
         ),
         "1" => array(
            "optgroup" => __("Asia Pacific", "EnGenius"),
            "options" => array(
               "0" => array(
                  "text" => __("English", "EnGenius"),
                  "href" => __("https://www.engeniustech.com.sg/?disable_geoip", "EnGenius")
               )
            )
         ),
         "2" => array(
            "optgroup" => __("Americas", "EnGenius"),
            "options" => array(
               "0" => array(
                  "text" => __("English", "EnGenius"),
                  "href" => __("https://www.engeniustech.com/?disable_geoip", "EnGenius")
               ),
               "1" => array(
                  "text" => __("Español", "EnGenius"),
                  "href" => __("https://www.engeniustech.com/es/?disable_geoip", "EnGenius")
               )
            )
         ),
         "3" => array(
            "optgroup" => __("Taiwan", "EnGenius"),
            "options" => array(
               "0" => array(
                  "text" => __("繁體中文", "EnGenius"),
                  "href" => __("https://www.engeniustech.com.tw/?disable_geoip", "EnGenius")
               )
            )
         )
      );
    }


    public function main() {
      if ($this->detection_plugin_not_loaded()) {
         return;
      }

      $this->info = $this->get_ip_info();
      $this->continent = strtoupper($this->info->continent->code);
      $this->country = $this->get_country();
      $this->browser_language = $this->get_browser_language();
      $this->isoCode = strtoupper($this->info->country->isoCode);
      $this->flag = $this->get_flag();

      if ($this->hide_notification()) {
         return;
      }
   
      $this->welcome_message = $this->get_welcome_message();
      $this->region_selection_message = $this->get_region_selection_message();
      $this->goto = $this->goto_message();
      $this->or = $this->or_message();
      $this->region_message = $this->get_region_message();
      
      $region = $this->goto_region();
      $this->region_label = $region['label'];
      $this->region_link = $region['link'];

      $this->delete_icon = '/wp-content/plugins/engenius-geoip/src/img/delete.svg';
   
      $this->load_assets();

      $this->render();
    }

    public function detection_plugin_not_loaded() {
        return !function_exists('geoip_detect2_get_info_from_current_ip');
    }

    public function hide_notification()
    {

      if (isset($_COOKIE['engenius_location_cookie']) && $_COOKIE['engenius_location_cookie']) {
         return true;
      }

      $queries = array();
      parse_str($_SERVER['QUERY_STRING'], $queries);
      $query = $queries['disable_geoip'];

      if (isset($query)) {
         setcookie('engenius_location_cookie', $this->browser_language, $this->cookie_time());
         return true;
      }

      if (isset($_SERVER['SERVER_NAME']) && $this->visitor_at_correct_domain($_SERVER['SERVER_NAME'])) {
         return $this->is_correct_site_language();
      }

      return false;
    }

   public function cookie_time() {
      return time() + (86400 * 30);
   }

   public function visitor_at_correct_domain($domain) {
      if (empty($this->continent)) {
         throw new \Exception("Continent is undefined"); 
      }

      switch($this->continent) {
         case 'NA':
            return $domain == 'www.engeniustech.com';
         case 'SA':
            return $domain == 'www.engeniustech.com';
         case 'AN':
            return $domain == 'www.engeniustech.com';
         case 'EU':
            return $domain == 'www.engeniusnetworks.eu';
         case 'AS':
            if (isset($this->isoCode) && $this->isoCode == 'TW') {
               return $domain == 'www.engeniustech.com.tw';
            }
            return $domain == 'www.engeniustech.com.sg';
         case 'AF':
            return $domain == 'www.engeniustech.com.sg';
         case 'OC':
            return $domain == 'www.engeniustech.com.sg';
      }

      return false;
   }

   public function is_correct_site_language() {
     if (defined('ICL_LANGUAGE_CODE')) {
         return $this->browser_language == strtolower(explode('-', ICL_LANGUAGE_CODE)[0]);
     }
     
     if ($this->get_url_language_prefix() === 'es') {
         return $this->browser_language == 'es';
     }

     return $this->browser_language == 'en';
   }

    public function get_ip_info() {
        return geoip_detect2_get_info_from_current_ip($locales = array('en'), $options = array());
    }

    public function get_url_language_prefix() {
        $path = $_SERVER['REQUEST_URI'];

        $url_segments = explode('/', $path);

        return $url_segments[1];
    }

    public function get_browser_language() {
        $langs = array();
     
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
           // break up string into pieces (languages and q factors)
           preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
     
           if (count($lang_parse[1])) {
              // create a list like "en" => 0.8
              $langs = array_combine($lang_parse[1], $lang_parse[4]);
              
              // set default to 1 for any without q factor
              foreach ($langs as $lang => $val) {
                    if ($val === '') $langs[$lang] = 1;
              }
     
              // sort list based on value	
              arsort($langs, SORT_NUMERIC);
           }
        }
     
        $valid_langs = array('en','zh','de','fr','es','it','nl','pl','ro','ru','sv');
     
        // look through sorted list and use first one that matches our languages
        foreach ($langs as $lang => $val) {
            $lang = explode('-', $lang)[0];
            $lang = strtolower($lang);
            
            if (in_array($lang, $valid_langs)) {
                return $lang;
            }
        }

        return 'en';
    }

    public function get_country() {
      if (isset($this->info->country->isoCode) && strtoupper($this->info->country->isoCode) == 'TW' && $this->browser_language == 'zh') {
         return '台灣';
      }
      return $this->info->country->names['en'];
    }

    public function goto_region() {
      if (empty($this->continent)) {
         throw new \Exception("Continent is undefined"); 
      }

      if (empty($this->isoCode)) {
         throw new \Exception("Country is undefined"); 
      }

      switch($this->continent) {
         case 'AF':
               return array(
                  'label' => 'EnGenius APAC (EN)',
                  'link' => 'https://www.engeniustech.com.sg/?disable_geoip'
               );
         case 'AN':
               return $this->get_americas_region($this->isoCode);
         case 'AS':
               if ($this->isoCode == 'TW') {
                  return array(
                     'label' => 'EnGenius TW (ZH-TW)',
                     'link' => 'https://www.engeniustech.com.tw/?disable_geoip'
                  );
               } else {
                  return array(
                     'label' => 'EnGenius APAC (EN)',
                     'link' => 'https://www.engeniustech.com.sg/?disable_geoip'
                  );
               }
         case 'EU':
               return $this->get_eu_region($this->isoCode);
         case 'NA':
               return $this->get_americas_region($this->isoCode);
         case 'OC':
               return array(
                  'label' => 'EnGenius APAC (EN)',
                  'link' => 'https://www.engeniustech.com.sg/?disable_geoip'
               );
         case 'SA':
               return $this->get_americas_region($this->isoCode);
      }
    }


    public function get_eu_region($isoCode) {
        switch($isoCode) {
            case 'DE':
                return array(
                    'label' => 'EnGenius (DE)',
                    'link' => 'https://www.engeniusnetworks.eu/de/?disable_geoip'
                );
            case 'ES':
                return array(
                    'label' => 'EnGenius (ES)',
                    'link' => 'https://www.engeniusnetworks.eu/es/?disable_geoip'
                );
            case 'FR':
                return array(
                    'label' => 'EnGenius (FR)',
                    'link' => 'https://www.engeniusnetworks.eu/fr/?disable_geoip'
                );
            case 'IT':
                return array(
                    'label' => 'EnGenius (IT)',
                    'link' => 'https://www.engeniusnetworks.eu/it/?disable_geoip'
                );
            // case 'NL':
            //     return array(
            //         'label' => 'EnGenius (NL)',
            //         'link' => 'https://www.engeniusnetworks.eu/nl/?disable_geoip'
            //     );
            // case 'PL':
            //     return array(
            //         'label' => 'EnGenius (PL)',
            //         'link' => 'https://www.engeniusnetworks.eu/pl/?disable_geoip'
            //     );
            // case 'RO':
            //    return array(
            //       'label' => 'EnGenius (RO)',
            //       'link' => 'https://www.engeniusnetworks.eu/ro/?disable_geoip'
            //    );
            case 'RU':
                return array(
                    'label' => 'EnGenius (RU)',
                    'link' => 'https://www.engeniusnetworks.eu/ru/?disable_geoip'
                );
            // case 'SV':
            //     return array(
            //         'label' => 'EnGenius (SV)',
            //         'link' => 'https://www.engeniusnetworks.eu/sv/?disable_geoip'
            //     );
            // case 'UK':
            //    return array(
            //          'label' => 'EnGenius (Ukraine)',
            //          'link' => 'https://www.engeniusnetworks.eu/uk/?disable_geoip'
            //    );
            case 'GB':
                return array(
                    'label' => 'EnGenius (United Kingdom)',
                    'link' => 'https://www.engeniusnetworks.eu/gb/?disable_geoip'
                );
            default:
                return array(
                    'label' => 'EnGenius EU',
                    'link' => 'https://www.engeniusnetworks.eu/?disable_geoip'
                );
        }
    }

    public function get_americas_region($isoCode) {
      if (isset($this->browser_language) && $this->browser_language == 'ES') {
         return array(
               'label' => 'EnGenius Americas (ES)',
               'link' => 'https://www.engeniustech.com/es/?disable_geoip'
         );
      }

      return array(
         'label' => 'EnGenius Americas (EN)',
         'link' => 'https://www.engeniustech.com/?disable_geoip'
      );
    }

   public function get_welcome_message() {
      switch ($this->browser_language) {
         case 'fr':
            return 'Bienvenue à EnGenius';
         case 'de':
            return 'Willkommen bei EnGenius';
         case 'it':
            return 'Benvenuti in EnGenius!';
         case 'ru':
            return 'Добро пожаловать в EnGenius!';
         case 'es':
            return '¡Bienvenido a EnGenius!';
        case 'zh':
            return '歡迎來到EnGenius!';
         default:
            return 'Welcome to EnGenius!';
      }
   }
  
   public function get_region_selection_message() {
      switch ($this->browser_language) {
         case 'fr':
            return 'Sélectionnez la langue de votre emplacement:';
         case 'de':
            return 'Wählen Sie die Sprache Ihres Standortes aus:';
         case 'it':
            return 'Seleziona la lingua della tua posizione:';
         case 'ru':
            return 'Выберите язык своего местоположения:';
         case 'es':
            return 'Seleccione el idioma de su región:';
         case 'zh':
            return '請選擇你所在地的語言，你來自 [TW]台灣:';
         default:
            return 'Select best site for your location.';
      }
   }

   public function goto_message() {
      switch ($this->browser_language) {
         case 'fr':
            return 'Continuez sur mon site régional:';
         case 'de':
            return 'Weiter zu meiner Regionalseite:';
         case 'it':
            return 'Continua sul sito del mio paese:';
         case 'ru':
            return 'Продолжить на моем региональном сайте:';
         case 'es':
            return 'Continuar hacia la Web de mi región:';
         case 'zh':
            return '[繼續] 前往此語言網站';
         default:
            return 'Go to';
      }
   }

   public function or_message() {
      switch ($this->browser_language) {
         case 'fr':
            return 'ou';
         case 'de':
            return 'oder';
         case 'it':
            return 'o';
         case 'ru':
            return 'или';
         case 'es':
            return 'o';
         case 'zh':
            return '或';
         default:
            return 'or';
      }
   }

   public function get_region_message() {
      switch ($this->browser_language) {
         case 'fr':
            return 'Changer de langue/région:';
         case 'de':
            return 'Sprache/Region ändern:';
         case 'it':
            return 'Cambia lingua/nazione:';
         case 'ru':
            return 'Изменить язык/регион:';
         case 'es':
            return 'Cambiar idioma/región:';
         case 'zh':
            return '更換其他語言/區域';
         default:
            return 'Select Region';
      }
   }

    public function get_flag() {
        $iso_countries = array(
            "DE" => "de",
            "FR" => "fr",
            "ES" => "es",
            "IT" => "it",
            "NL" => "nl",
            "PL" => "pl",
            "RO" => "ro",
            "RU" => "ru",
            "SV" => "se",
            "GB" => "gb",
            "SG" => "sg",
            "US" => "us",
            "TW" => "tw",
            "EU" => "eu"
        );

        if (isset($iso_countries[$this->isoCode])) {
            return $iso_countries[$this->isoCode];
        }

        if ($this->continent == 'EU') {
            return $iso_countries['EU'];
        }

        return 'globe';
    }

    public function sanitize_xss($value) {
        return htmlspecialchars(strip_tags($value));
    }


   public function load_assets() {
      if (!function_exists('wp_enqueue_script') || !function_exists('wp_enqueue_style')) {
         throw new \Exception('Error loading assets: WordPress functions do not exist.');
      }

      wp_enqueue_script('geoip-header-headroom', '/wp-content/plugins/engenius-geoip/src/lib/headroom.min.js');
      wp_enqueue_script('geoip-header-select2-js', '/wp-content/plugins/engenius-geoip/src/lib/select2.min.js');
      wp_enqueue_script('geoip-header-js', '/wp-content/plugins/engenius-geoip/src/js/script.js');
      wp_enqueue_style('geoip-header-select2-css', '/wp-content/plugins/engenius-geoip/src/lib/select2.min.css');
      wp_enqueue_style('geoip-header-css', '/wp-content/plugins/engenius-geoip/src/css/style.css');

      wp_enqueue_style('geoip-header-css-flag', '/wp-content/plugins/engenius-geoip/node_modules/flag-icon-css/css/flag-icon.css');

      if ($this->is_la_branch()) {
         wp_enqueue_style('geoip-header-css-la', '/wp-content/plugins/engenius-geoip/src/css/style_la.css');
      }
   }

   public function is_la_branch() {
      return $_SERVER['HTTP_HOST'] == 'engeniustech.com';
   }

   public function render() {
         ?>
            <div id="location-header-container" class="close w-100" data-language="<?php echo $this->sanitize_xss($this->iso_code); ?>">
               <div class="outer d-flex">
                  <div class="inner w-100 w-lg-auto d-flex align-items-center flex-sm-wrap flex-lg-nowrap justify-content-between">
                     <span class="welcome d-flex justify-content-between align-items-center">
                        <span class="d-flex flex-column">
                           <?php if ($this->is_la_branch()) { ?>
                              <div class="font-13 selection-message"><?php echo $this->sanitize_xss($this->region_selection_message); ?></div>
                           <?php } else { ?>
                              <div class="font-15 welcome"><?php echo $this->sanitize_xss($this->welcome_message); ?></div>
                              <div class="font-15 selection-message"><?php echo $this->sanitize_xss($this->region_selection_message); ?></div>
                           <?php } ?>
                        </span>
                        <div class="d-flex align-items-center close-geoip">
                           <img src="<?php echo $this->sanitize_xss($this->delete_icon); ?>">
                        </div>
                     </span>
                     <div class="d-flex justify-content-between location">
                        <div class="column current-location">
                           <?php if ($this->flag) { ?>
                              <?php if ($this->flag == 'globe') { ?>
                                 <img class="location-header-flag" src="'/wp-content/plugins/engenius-geoip/src/img/globe.svg'" />
                              <?php } else { ?>
                                 <span class="mr-3 flag-icon flag-icon-<?php echo $this->sanitize_xss($this->flag); ?>"></span>
                              <?php } ?>
                           <?php } ?>
                           <?php if ($this->is_la_branch()) { ?>
                              <div class="font-13"><?php echo $this->sanitize_xss($this->country); ?></div>
                           <?php } else { ?>
                              <div class="font-14"><?php echo $this->sanitize_xss($this->country); ?></div>
                           <?php } ?>
                        </div>
                        <div class="current-location-button">
                           <a href="<?php echo $this->sanitize_xss($this->region_link); ?>">
                              <?php if ($this->is_la_branch()) { ?>
                                 <button>
                                    <span class="font-13"><?php echo $this->sanitize_xss($this->goto); ?></span>
                                    <span class="font-13 font-weight-bold"><?php echo $this->sanitize_xss($this->region_label); ?></span>
                                 </button>
                              <?php } else { ?>
                                 <button>
                                    <span class="font-14"><?php echo $this->sanitize_xss($this->goto); ?></span>
                                    <span class="font-15 font-weight-bold"><?php echo $this->sanitize_xss($this->region_label); ?></span>
                                 </button>
                              <?php } ?>
                           </a>
                        </div>
                     </div>
                     <div id="open-select2-mobile" class="chevron-container">
                        <span class="chevron bottom"></span>
                     </div>
                     <div class="d-none d-lg-flex align-items-center margin-20">
                        <span class="font-13"><?php echo $this->sanitize_xss($this->or); ?></span>
                     </div>
                     <div class="d-none d-lg-flex align-items-center">
                        <div class="mr-3">
                           <select id="engenius-geoip-select">
                              <option><?php echo $this->sanitize_xss($this->region_message); ?></option>
                              <?php
                                 foreach ($this->regions as $region) {
                              ?>
                                 <optgroup label="<?php echo $this->sanitize_xss(__($region['optgroup'], 'EnGenius')); ?>">
                                    <?php
                                       foreach ($region['options'] as $option) {
                                    ?>
                                       <option value="<?php echo $this->sanitize_xss(__($option['href'], 'EnGenius')); ?>">
                                          <div><?php echo $this->sanitize_xss(__($option['text'], 'EnGenius')); ?></div>
                                       </option>
                                    <?php
                                       }
                                    ?>
                                 </optgroup>
                              <?php
                                 }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="d-none d-lg-flex close-geoip">
                     <img src="<?php echo $this->sanitize_xss($this->delete_icon); ?>">
                  </div>
               </div>
            </div>
         <?php
    }



}