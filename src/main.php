<?php

   function engenius_add_geoip_header() {
      try {
         $geoip = new EnGenius\EnGeniusGeoip();
         $geoip->main();
      } catch (Exception $e) {
         return;
      }
   }

   if (!function_exists('add_action') || !function_exists('wp_body_open')) {
      return;
   }

   add_action('wp_body_open', 'engenius_add_geoip_header');